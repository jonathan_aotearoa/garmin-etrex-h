# QGIS

## Creating a new project

1. In the main menu, select 'Project > New'
1. Add a Topo50 Map layer
    1. In the Browser pane expand 'WMS > Linz Topo 50'
    1. Right-click 'NZ Topo50 Maps' and select 'Add Layer'
    1. In the Layers pane, right-click the newly added layer and select 'Select Layer CRS'
    1. In the 'Coordinate Reference System Selector' dialog select 'NZGD2000' and click OK
1. Save the project

## Creating a new Shapefile layer

1. In the main menu, select 'Layer > Create Layer > New Shapefile Layer...'
1. In the 'New Vector Layer' dialog set the following values:
    * Type: Line
    * CRS: NZGD2000
1. Add the following attributes:
   * Name: name, Type: String, Width: 80
   * Name: desc, Type: String, Width: 80
   * Name: length m, Type: Decimal, Width: 20, Precision: 2
   * Name: length km, Type: Decimal, Width: 20, Precision: 2
1. Click OK
1. Enter a file name in the 'Save layer as...' dialog and click OK
1. Save the project

* If the Nz Topo50 Maps layer disappears right-click it and select 'Zoom to Layer'
* To centre on Arthur's Pass enter the coordinates: 171.56,-42.94

## Creating a new layer from a LINZ GeoTIFF

1. Go to the [LINZ Topo Map Chooser](https://www.linz.govt.nz/land/maps/linz-topographic-maps/map-chooser)
1. Click on the appropriate Topo250 sheet
1. Click on the appropriate Topo50 sheet
1. Click the download GeoTIF link
1. In QGIS go to "Layer > Add Layer > Add Raster Layer"
1. Select the previously downloaded GeoTIFF file and click Open

## Adding a linear feature

1. Create a new shape layer
    1. Set the type to "Line"
    1. Set the data source encoding to "System"
    1. Set the coordinate reference system to "New Zealand Transverse Mercator 2000"
1. Select the newly added layer and toggle editing on
1. Click the "Add Feature" button
1. Click on the layer to add a waypoint
1. When the route has been traced right click and enter a name in "Feature Attributes" dialog
1. Click "OK" to create the feature, or "Cancel" to discard it
1. Click the "Current Edits" button and click "Save for Selected Layer(s)"
1. Save the project

## Calculating line length

1. Right click on the layer in the "Layers" panel
1. Select "Open Attribute Table" from the context menu
1. Click the "Open field calculator" button
1. Select the "Update existing field" checkbox if your length field already exists
1. In the "Expression" text area input "$length / 1000"

## Transferring Waypoints to a GPS

N.B. This currently doesn't work

1. Select the "Vector - GPS - GPS Tools" menu item
1. Select the "Upload to GPS" tab
1. Select the appropriate "Data Layer"

## Saving Waypoints to a GPX File

1. Right click the Waypoints layer
1. Select "Save As" from the context menu
1. Select "GPS eXchange Format [GPX]" as the Format
1. Select "Layer CRS" "EPSG:4326,WGS 84" as the CRS
1. Enter a file name and click Save