# Garmin eTrex H Workflow

![Garmin eTrex H](docs/images/readme/etrex-h.jpg)

## Overview

I recently started using my old [Garmin eTrex H](https://buy.garmin.com/en-GB/GB/sports-recreation/discontinued/etrex-h/prod8705.html) for winter tramping trips. The eTrex H is a great GPS but manually entering waypoints is a slow process. I therefore wanted to establish a straightforward workflow for transering data to/from the device using freely available open source software. An additional requirement was the ability to overlay waypoints on [LINZ topographical data](https://data.linz.govt.nz/).

* **GPS**: [Garmin eTrex H](https://buy.garmin.com/en-GB/GB/sports-recreation/discontinued/etrex-h/prod8705.html)
* **OS**: [Ubuntu 14.04](http://releases.ubuntu.com/14.04/)

## Contents

* [Connecting](#markdown-header-connecting)
* [GPSBabel 1.4.3](#markdown-header-gpsbabel)
* [QGIS 2.0.1](#markdown-header-qgis)

## Connecting

### Hardware

I'm using the following cable and serial-USB coverter to connect the GPS unit to my computer:

* RS-232 Serial Port Connector Cable
* [RS-232 DB9m to USB Converter](http://www.jaycar.co.nz/rs-232-db9m-to-usb-converter/p/XC4927)

### Serial-USB Converter Setup

#### Prerequisites

* Ensure you have access to serial ports by adding yourself to the 'dialout' group in Ubuntu.

#### Loading the USB Serial Driver

Plug in the serial-USB converter and run the following commands:

```sh
# Check the USB serial converter is connected as a USB device
lsusb | grep -i 'PL2303'
# Output: Bus 002 Device 006: ID 067b:2303 Prolific Technology, Inc. PL2303 Serial Port

# Load the driver (the vendor and product IDs are derived from the lsusb output)
modprobe usbserial vendor=0x067b product=0x2303

# Verify the converter is attached by checking the kernel message buffer
dmesg | grep -i 'PL2303'
# Output: [  690.042510] usb 2-1.1: pl2303 converter now attached to ttyUSB0
```

Run the following command if you want to query the device information (N.B. you can use the DEVLINKS property value when configuring the eTrex H as an input source in GPSBabel):

```sh
# 'ttyUSB0' is the terminal listed in the dmesg command output
udevadm info /dev/ttyUSB0
```

Output:

```sh
P: /devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.1/2-1.1:1.0/ttyUSB0/tty/ttyUSB0
N: ttyUSB0
S: serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller_D-if00-port0
S: serial/by-path/pci-0000:00:1d.0-usb-0:1.1:1.0-port0
E: DEVLINKS=/dev/serial/by-id/usb-Prolific_Technology_Inc._USB-Serial_Controller_D-if00-port0 /dev/serial/by-path/pci-0000:00:1d.0-usb-0:1.1:1.0-port0
E: DEVNAME=/dev/ttyUSB0
E: DEVPATH=/devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.1/2-1.1:1.0/ttyUSB0/tty/ttyUSB0
E: ID_BUS=usb
E: ID_MM_CANDIDATE=1
E: ID_MODEL=USB-Serial_Controller_D
E: ID_MODEL_ENC=USB-Serial\x20Controller\x20D
E: ID_MODEL_FROM_DATABASE=PL2303 Serial Port
E: ID_MODEL_ID=2303
E: ID_PATH=pci-0000:00:1d.0-usb-0:1.1:1.0
E: ID_PATH_TAG=pci-0000_00_1d_0-usb-0_1_1_1_0
E: ID_REVISION=0400
E: ID_SERIAL=Prolific_Technology_Inc._USB-Serial_Controller_D
E: ID_TYPE=generic
E: ID_USB_DRIVER=pl2303
E: ID_USB_INTERFACES=:ff0000:
E: ID_USB_INTERFACE_NUM=00
E: ID_VENDOR=Prolific_Technology_Inc.
E: ID_VENDOR_ENC=Prolific\x20Technology\x20Inc.\x20
E: ID_VENDOR_FROM_DATABASE=Prolific Technology, Inc.
E: ID_VENDOR_ID=067b
E: MAJOR=188
E: MINOR=0
E: SUBSYSTEM=tty
E: USEC_INITIALIZED=9148703
```

## GPSBabel

The [GPSBabel 1.4.3](https://packages.ubuntu.com/precise/utils/gpsbabel-gui) package supports transfering route, track and waypoint data to/from a wide range of GPS units. The following screenshots illustrate adding the Garmin eTrex H as an input source. This assumes you've already followed the [connection instructions](#markdown-header-connecting).

#### Open GPSBabel
![Screenshot 1](docs/images/gpsbabel/screenshot-1.png)
#### Add Source
![Screenshot 2](docs/images/gpsbabel/screenshot-2.png)
#### Source Configuration
![Screenshot 3](docs/images/gpsbabel/screenshot-3.png)

## QGIS

[QGIS](https://www.qgis.org) is a fully featured open source Geographical Information System (GIS). There are other, more lightweight, .gpx file visualisation tools available (e.g. [Viking GPS data editor and analyzer](https://sourceforge.net/p/viking/wikiallura/Main_Page/)). However, none of them work as seamlessly with [LINZ topographical data](https://data.linz.govt.nz/) as QGIS. I also refuse to install any software hosted on Sourceforge due to [bundled malware](https://en.wikipedia.org/wiki/SourceForge#Project_hijackings_and_bundled_malware).

### Configuring Access to LINZ Data

1. Register with [LINZ](https://data.linz.govt.nz/)
2. Generate an [API key](https://data.linz.govt.nz/my/api/)
3. Add a WMS/WMTS Layer in QGIS using the following URL: `https://data.linz.govt.nz/services;key=YOUR_API_TOKEN/wmts/1.0.0/layer/767/WMTSCapabilities.xml`

#### Tile Sets

* *WGS84 Web Mercator (EPSG:3857)* - Most suitable for web mapping applications, and is compatible with the Google Map, OSM and Bing Maps tiles without need for re-projection or scaling.
* *NZTM2000 (EPSG:2193)* - Most useful for New Zealand geospatial professionals and can be used for desktop GIS mapping. It is also recommended for New Zealand mainland web applications when area and distances need to be well represented on the map.

### Working with GPS Data

1. Install/enable the [GPS plugin](https://docs.qgis.org/2.0/en/docs/user_manual/working_with_gps/plugins_gps.html)

![Screenshot 1](docs/images/qgis/screenshot-1.png)

### Useful Links

* [Using LDS WMTS in QGIS](http://www.linz.govt.nz/data/linz-data-service/guides-and-documentation/using-lds-wmts-in-qgis)
